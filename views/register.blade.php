<!DOCTYPE html>
<html>
<head>
    <title>GodChryz|Register</title>
    <link rel="stylesheet" href="style.css">
    <style type="text/css">
        * { margin: 0px; padding: 0px; }
body {
    font-size: 120%;
    background: #002b36;
    color: #93a1a1;
}
.header {
    width: 40%;
    margin: 50px auto 0px;
    color: #268bd2;
    background: #eee8d5;
    text-align: center;
    border: 1px solid cyan;
    border-bottom: none;
    border-radius: 10px 10px 0px 0px;
    padding: 20px;
}
form, .content {
    width: 40%;
    margin: 0px auto;
    padding: 20px;
    border: 1px solid cyan;
    background: #073642;
    border-radius: 0px 0px 10px 10px;
}
.input-group {
    margin: 10px 0px 10px 0px;
}
.input-group label {
    display: block;
    text-align: left;
    margin: 3px;
}
.input-group input {
    color: green;
    height: 30px;
    width: 93%;
    padding: 5px 10px;
    font-size: 16px;
    border-radius: 5px;
    border: 1px solid cyan;
    background: transparent;
}
#user_type {
    color: green;
    height: 40px;
    width: 98%;
    padding: 5px 10px;
    font-size: 16px;
    border-radius: 5px;
    border: 1px solid cyan;
    background: transparent;
}
.btn {
    padding: 10px;
    font-size: 15px;
    color: white;
    background: #5F9EA0;
    border: none;
    border-radius: 5px;
}
.error {
    width: 92%; 
    margin: 0px auto; 
    padding: 10px; 
    border: 1px solid #a94442; 
    color: #a94442; 
    background: #f2dede; 
    border-radius: 5px; 
    text-align: left;
}
.success {
    color: #3c763d; 
    background: #dff0d8; 
    border: 1px solid #3c763d;
    margin-bottom: 20px;
    text-align: center;
}
.profile_info img {
    display: inline-block; 
    width: 50px; 
    height: 50px; 
    margin: 5px;
    float: left;
}
.profile_info div {
    display: inline-block; 
    margin: 5px;
    color: #268bd2;
}
.logout {
    background-color: #002b36;
    border-color: grey;
    color: grey;
    padding: 3px;
    margin: 5px;
}
.logout:hover {
    background-color: #002b36;
    border-color: #93a1a1;
    color: #93a1a1;
    padding: 3px;
    margin: 5px;
}
.add {
    background-color: #002b36;;
    border-color: green;
    color: green;
    padding: 3px;
    margin: 5px;
}
.add:hover {
    background-color: #002b36;;
    border-color: #859900;
    color: #859900;
    padding: 3px;
    margin: 5px;
}
.delete {
    background-color: #002b36;;
    border-color: #dc322f;
    color: #dc322f;
    padding: 3px;
    margin: 5px;
}
.delete:hover {
    background-color: #002b36;;
    border-color: red;
    color: red;
    padding: 3px;
    margin: 5px;
}
.update {
    background-color: #002b36;;
    border-color: #6c71c4;
    color: #6c71c4;
    padding: 3px;
    margin: 5px;
}
.update:hover {
    background-color: #002b36;;
    border-color: violet;
    color: violet;
    padding: 3px;
    margin: 5px;
}
.view {
    background-color: #002b36;;
    border-color: #2aa198;
    color: #2aa198;
    padding: 3px;
    margin: 5px;
}
.view:hover {
    background-color: #002b36;;
    border-color: cyan;
    color: cyan;
    padding: 3px;
    margin: 5px;
}
.profile_info:after {
    content: "";
    display: block;
    clear: both;
}
table {
    border: 2px solid cyan;
    width: 90%;
    margin: 50px auto 0px;
    color: #268bd2;
    background: transparent;
    text-align: center;
    border-radius: 10px 10px 0px 0px;
    padding: 20px;
    border-bottom: none;
}
table .deletehead {
    font-size: 50px;
    color: #dc322f;
    border: 2px solid #dc322f;
    border-radius: 10px 10px 0px 0px;
    border-bottom: none;
}
table .viewhead {
    font-size: 50px;
    color: #2aa198;
    border: 2px solid #2aa198;
    border-radius: 10px 10px 0px 0px;
    border-bottom: none;
}
table .rows {
    color: #d33682;
}
table .delete_btn {
    background-color: transparent;
    border-color: #dc322f;
    color: #dc322f;
}
table .delete_btn:hover {
    background-color: transparent;
    border-color: red;
    color: red;
}
td {
    background-color: #073642;
}
table .update_btn {
    background-color: transparent;
    border-color: #6c71c4;
    color: #6c71c4;
}
table .update_btn:hover {
    background-color: transparent;
    border-color: violet;
    color: violet;
}
table .updatehead {
    font-size: 50px;
    color: #6c71c4;
    border: 2px solid #6c71c4;
    border-radius: 10px 10px 0px 0px;
    border-bottom: none;
}
.userupdate {
    background: transparent;
    border: none;
    color: #268bd2;
}
.userupdate:hover {
    background: transparent;
    border: none;
    color: cyan;
}

.deletetooltip {
    display: none;
}

.deletetooltip:hover {
    color: red;
}
    </style>
</head>
<body>
<div class="header">
    <h2>Register</h2>
</div>
<form method="POST" action="register.php">

    <div class="input-group">
        <label>Username</label>
        <input type="text" name="username" >
    </div>
    <div class="input-group">
        <label>Email</label>
        <input type="email" name="email">
    </div>
    <div class="input-group">
        <label>Password</label>
        <input type="password" name="password_1">
    </div>
    <div class="input-group">
        <label>Confirm password</label>
        <input type="password" name="password_2">
    </div>
    <div class="input-group">
        <button type="submit" class="btn" name="register_btn">Register</button>
    </div>
    <p>
        Already a member? <a href="login.php">Sign in</a>
    </p>
</form>
</body>
</html>