
<!DOCTYPE html>
<html>
<head>
	<title> Landing Page </title>
	<link rel="stylesheet" type="text/css" href="form.css">
</head>
<style >
	*{  
  margin:0;
  padding:0;
}

h1 {
  font-size: 2em;
  font-family: "Calibri";
  font-weight: normal;
  margin: .67em 0;
  display: block;
}

.registered {
    position: relative;
    width:100%;
    height: 50%;
}

.registered img {
    margin-bottom: 0px;
    width: 100px;
    height: 100px;
}



img {
    margin-bottom: 20px;
}

.avatar {
    margin: 10px 0 20px 0;
}

.module{
  position:relative;  
  height:65%;
  width:450px;
  margin-left:auto;
  margin-right:auto;
}

.user {
    color: #1abc9c;
    font-weight: bold;
}

.userlist {
    float:left;
    padding: 30px;
}

.userlist span {
    color: #1abc9c;
    font-weight: bold;

}

.welcome{
  position:relative;
  top:30%;    
  height:65%;
  width:900px;
  margin-left:auto;
  margin-right:auto;
  margin-top: 50px;
}

::-moz-selection {
  background: #19547c;
}
::selection {
  background: #19547c;
}
input::-moz-selection {
  background: #037db6;
}
input::selection {
  background: #037db6;
}

body{
  color: white;
  background-color:white;
  font-family:helvetica;
  background:url('jpg') #eeeeee no-repeat center top;
  background-size: 100% 190%;
}

.body-content{
  position:relative;
  top:10px;
  height:auto;
  width:95%;
  margin-left:auto;
  margin-right:auto; 
  /*background: transparent;*/
  background-color:white; 
  box-shadow: 0 0 80px grey ;
 
}
.body-content2{
  top:10px;
  height:10px;
  width:95%;
  margin-left:auto;
  margin-right:auto; 
  /*background: transparent;*/
  background-color:white; 
  box-shadow: 0 0 80px grey ;

}


select,
textarea,
input[type="text"],
input[type="password"],
input[type="email"]
{
  height:30px;
  width:100%;;
  display: inline-block;
  vertical-align: middle;
  height: 34px;
  padding: 0 10px;
  margin-top: 3px;
  margin-bottom: 10px;
  font-size: 15px;
  line-height: 20px;
  border: 1px solid rgba(255, 255, 255, 0.3);
  background-color: rgba(0, 0, 0, 0.5);
  color: rgba(255, 255, 255, 0.7);
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  border-radius: 2px;
}

select,
textarea,
input[type="text"],
input[type="password"],
input[type="email"] {
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  appearance: none;
  -webkit-transition: background-position 0.2s, background-color 0.2s, border-color 0.2s, box-shadow 0.2s;
  transition: background-position 0.2s, background-color 0.2s, border-color 0.2s, box-shadow 0.2s;
}
select:hover,
textarea:hover,
input[type="text"]:hover,
input[type="password"]:hover,
input[type="email"]:hover {
  border-color: rgba(255, 255, 255, 0.5);
  background-color: rgba(0, 0, 0, 0.5);
  color: rgba(255, 255, 255, 0.7);
}
select:focus,
textarea:focus,
input[type="text"]:focus,
input[type="password"]:focus,
input[type="email"]:focus {
  border: 2px solid;
  border-color: #1e5f99;
  background-color: rgba(0, 0, 0, 0.5);
  color: #ffffff;
}
.btn {
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  margin: 3px 0;
  padding: 6px 20px;
  font-size: 15px;
  line-height: 20px;
  height: 34px;
  background-color: rgba(0, 0, 0, 0.15);
  color: #00aeff;
  border: 1px solid rgba(255, 255, 255, 0.15);
  box-shadow: 0 0 rgba(0, 0, 0, 0);
  border-radius: 2px;
  -webkit-transition: background-color 0.2s, box-shadow 0.2s, background-color 0.2s, border-color 0.2s, color 0.2s;
  transition: background-color 0.2s, box-shadow 0.2s, background-color 0.2s, border-color 0.2s, color 0.2s;
}
.btn.active,
.btn:active {
  padding: 7px 19px 5px 21px;
}
.btn.disabled:active,
.btn[disabled]:active,
.btn.disabled.active,
.btn[disabled].active {
  padding: 6px 20px !important;
}
.btn:hover,
.btn:focus {
  background-color: rgba(0, 0, 0, 0.25);
  color: #ffffff;
  border-color: rgba(255, 255, 255, 0.3);
  box-shadow: 0 0 rgba(0, 0, 0, 0);
}
.btn:active,
.btn.active {
  background-color: rgba(0, 0, 0, 0.15);
  color: rgba(255, 255, 255, 0.8);
  border-color: rgba(255, 255, 255, 0.07);
  box-shadow: inset 1.5px 1.5px 3px rgba(0, 0, 0, 0.5);
}
.btn-primary {
  background-color: #098cc8;
  color: #ffffff;
  border: 1px solid transparent;
  box-shadow: 0 0 rgba(0, 0, 0, 0);
  border-radius: 2px;
  -webkit-transition: background-color 0.2s, box-shadow 0.2s, background-color 0.2s, border-color 0.2s, color 0.2s;
  transition: background-color 0.2s, box-shadow 0.2s, background-color 0.2s, border-color 0.2s, color 0.2s;
  background-image: -webkit-linear-gradient(top, #0f9ada, #0076ad);
  background-image: linear-gradient(to bottom, #0f9ada, #0076ad);
  border: 0;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.3), 0 0 0 1px rgba(255, 255, 255, 0.15) inset;
}
.btn-primary:hover,
.btn-primary:focus {
  background-color: #21b0f1;
  color: #ffffff;
  border-color: transparent;
  box-shadow: 0 0 rgba(0, 0, 0, 0);
}
.btn-primary:active,
.btn-primary.active {
  background-color: #006899;
  color: rgba(255, 255, 255, 0.7);
  border-color: transparent;
  box-shadow: inset 1.5px 1.5px 3px rgba(0, 0, 0, 0.5);
}
.btn-primary:hover,
.btn-primary:focus {
  background-image: -webkit-linear-gradient(top, #37c0ff, #0097dd);
  background-image: linear-gradient(to bottom, #37c0ff, #0097dd);
}
.btn-primary:active,
.btn-primary.active {
  background-image: -webkit-linear-gradient(top, #006ea1, #00608d);
  background-image: linear-gradient(to bottom, #006ea1, #00608d);
  box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.6) inset, 0 0 0 1px rgba(255, 255, 255, 0.07) inset;
}
.btn-block {
  display: block;
  width: 100%;
  padding-left: 0;
  padding-right: 0;
}

.alert {
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  padding: 4px 20px 4px 20px;
  font-size: 13px;
  line-height: 20px;
  margin-bottom: 20px;
  text-shadow: none;
  position: relative;
  background-color: #272e3b;
  color: rgba(255, 255, 255, 0.7);
  border: 1px solid #000;
  box-shadow: 0 0 0 1px #363d49 inset, 0 5px 10px rgba(0, 0, 0, 0.75);
}
.alert-error {
  color: #f00;
  background-color: #360e10;
  box-shadow: 0 0 0 1px #551e21 inset, 0 5px 10px rgba(0, 0, 0, 0.75);
}
.alert:empty{
    display: none;
}
.alert-success {
  color: #21ec0c;
  background-color: #15360e;
  box-shadow: 0 0 0 1px #2a551e inset, 0 5px 10px rgba(0, 0, 0, 0.75);
}
.modal{
  margin-top: 5%;
  background-color: #405060;
  height: 75%;
  border-radius: 2%;
  padding: 3px 25px 3px 25px;
  box-shadow: 0 0 0 1px #2a551e inset, 0 5px 10px rgba(0, 0, 0, 0.75);
}
.img{
  height: 70%;
  width: 100%;
 
}
div.polaroid{
  width: 8.1%;
  background-color: white;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  margin-bottom: 25px;
  float: left;
  overflow: hidden;
  margin-right: 15px;
}
div.pol-conrainer{
  text-align: center;
  padding: 10px 20px;
}

.ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background: transparent;

}

li {
    float: left;
}

li a {
    display: block;
    color: black;
    font-weight: bold;
    text-align: center;
    padding: 23px 25px;
    text-decoration: none;
    border-radius: 2%;
}

li a:hover:not(.active) {
    background-color: lightgray;
}

.actiave {
    background-color: #1abc9c;
    color:white;

}
.logout{
    float: right;
   overflow: hidden;

}

.name{
  margin-right: 2%;
  float: right;
  overflow: hidden;
  text-align: center;
  margin-top: 21px;
  border-bottom: 1px solid #1abc9c;

}
.body-cut{
  height:60%;
  width:100%;
  margin-left:auto;
  margin-right:auto; 
  /*background: transparent;*/
  background-color: #405060; 
  box-shadow: 0 0 20px grey ;

}
.main-content{
  position: relative;
  height:100%;
  width:70%;
  margin-left: 25%;
  /*background: transparent;*/
}
.margin{
  margin-left:20px;
  margin-right:20px; 
}
.profile{
  position:relative;
  top:15px;
  left:10px;
  height:90%;
  width:20%;
  /*background: transparent;*/
  background-color: white; 
  box-shadow: 0 0 30px  gray;
  float: left; 
}
.profile-name{
    color:black;
    text-align: center;
    font-weight: bold;
    margin:auto;
    padding: 10%;
    width: 50%;
}
.textarea{
  width: 94%;
  height: 70%;
  border: 3px solid #cccccc;
  padding: 5px;
  border-radius: 10px;
  background-color: #818181;
}
.top-question{
  margin-left: 13%;
  color:black;
   height:200px;
  width:80%;
  /*background: transparent;*/
  border:3px solid gray;
  border-radius: 30px;
  background-color:#1abc9c; 
}
.registeredq{
    position: relative;
    width:14%;
    height: 50%;
    float: left;

}

.registeredq img {
    margin-bottom: 0px;
    width: 100px;
    height: 100px;
    float: left;
}
/* The Modal (background) */
.the-modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    margin-left: 26%;
    width: 50%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background: transparent;
}

/* Modal Content */
.the-modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;

}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.button{
  border: none;
  background-color: transparent;
  color: red;
  font-weight: bold;
  font-size: 17px;
}
</style>
<body>

	<div class="body-content">
		<div>
		<ul class="ul">
  				<li><a class="active" href="HomePage.php">Edit Account</a></li>
  				<li class="logout.php"><a href="login1.php">Log out</a></li>
   				<span class="user name" ><span style="color:black"> Welcome</span>  </span> 
			</ul>
		</div>
		<h1 class="margin user">Your question</h1>
			<div class="registeredq  margin">		
				<img class="polaroid" src=" ">
				<div class="pol-container"><span class='user'>  </span>
				 </div><br><br><br>			
			</div>
			<div class="top-question">
					<h4><i>@?></i></h4>
				</div>
				<br>
					<a href="login1.php"><button class="btn btn-primary" style="float: right;margin-right: 60px;"> Edit </button></a>
				<br><br><br>			
	</div>
		<div style="margin-top: 10px;" class="body-content">
			<br>
				<h1 style="text-align: center;color:black;" class="margin user">Top question</h1>
				<div class='registeredq  margin'>";		
				<img style='margin-left: 80px;height: 50px;width: 50px;border-radius: 50%;' class='polaroid' src='">
				<br><br><br> </div>";
				<div class='top-question'>";
				<h4><i><span style='color:black;' class='user'> </span>  </i></h4>
				</div> <br>"

	</div>


  

</body>
</html>