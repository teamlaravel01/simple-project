<!DOCTYPE html>
<head>
	<title>CodeShare</title>
</head>
<style>
.navbar {
    overflow: hidden;
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.8); /* Black w/ opacity */
    font-family: wh-cond,"Arial Narrow",sans-serif;


}
.navbara {
    float: left;
    font-size: 20px;
    color: white;
    text-align: center;
    padding: 30px 35px;
    text-decoration: none;
}

.dropdown {
    float: left;
    overflow: hidden;
}

.dropdown .dropbtn {
    font-size: 20px;    
    border: none;
    outline: none;
    color: white;
    padding: 30px 35px;
    background-color: inherit;
    font-family: inherit;
    letter-spacing: 3px;

}

.navbara:hover, .dropdown:hover .dropbtn {
    background-color: gray;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    float: none;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {
    background-color: #ddd;
}

.dropdown:hover .dropdown-content {
    display: block;
}

.body-content { 
  position:relative;
  top:0px;
  height:auto;
  width:auto;
  background-color:white; 
}
.body-img { 
  position:relative;
  top:0px;
  height:600px;
  width:auto;
  /*background: transparent;*/
  background:url('img/img1.jpg') #eeeeee no-repeat center top;
  background-size: 100% 600px;
  background-color:#4b4b4b; 
}

body {
  color: white;
  background-color:#f1f1f1;
  background-size: 100% 190%;
  width: 100%;
  margin:auto;
  font-family:wh-cond,"Arial Narrow",sans-serif; 
}
.btn-login{

    position: relative;   
    border: none;
    outline: none;
    color: white;
    padding: 15px 35px;
    background-color: inherit;
    background-color: #595959;
    border-radius: 4px;
    font-size: 16px;
}
.btn-sign-up{

    position: relative;
    border: none;
    outline: none;
    color: white;
    padding: 15px 35px;
    background-color: inherit;
    background-color: #595959;
    border-radius: 4px;
    font-size: 17px;
    letter-spacing: 2px;

}
.sign {
    float: right;
    color:khaki;
    text-decoration: none;
}
.sign:hover {
    background-color: #aaa;

}
.nav-div {

    position: relative;       
    border: none;
    outline: none;
    color: white;
    padding: 17.5px 8px;
    background-color: inherit;
    float: right;
    margin: auto;

}

.body-info {

    height: auto;
    width: 70%;
    background-color: white;
    margin-left: 3%;
    border-radius: 13px;
    margin-top: 30px;

}
.body-info1 {

    height: 150px;
    width: 70%;
    background-color: white;
    margin-left: 3%;
    border-radius: 13px;
    margin-top: 150px;
}
.about {
    background-color: #7abd13;
    padding: 15px 19px;
    color:white;
    padding-top: 25px;
    border-bottom-right-radius: 11px;
    letter-spacing: 6px;
    font-family:  wh-cond,"Arial Narrow",sans-serif;
    font-size: 19px;
}
.posts {
    background-color: #7abd13;
    padding: 19px ;
    width: auto;
    color:white;
    letter-spacing: 6px;
    font-family:  wh-cond,"Arial Narrow",sans-serif;
    font-size: 19px;
    font-weight: bold;
    text-align: left;
}
.margin-left {

        margin-left: 35px;
}
.margin-left2{
        
        margin-left: 150px;
}
.h1{
    color:black;
    letter-spacing: 5px;
    margin-bottom: 0px;
}
.p2{
    color:#585858;
    letter-spacing: 2px;    
    word-spacing: 2px ;
    font-size: 23px;
}
.p{
    color:gray;
    margin-top:-10px; 
}

/*THIS IS FOR THE LOGIN MODAL*/

/* The Modal (background) */
.the-modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 80px;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.the-modal-content {

    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(100,100,100,0.4); /* Black w/ opacity */
    margin: auto;
    padding: 20px;
    width: 40%;
    border-radius: 4px;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.modal-login{
    display: block;
    vertical-align: middle;
    margin-bottom: 10px;
}
.input{
    margin-bottom: 13px;
    width: 100%;
    color:white;
    height: 45px;
    font-size: 19px;
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    border:1px solid gray;
}

.submit{
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 13px;
    background-color:#7abd13 ;
    width: 100%;
    height: 45px;
    border-radius: 4px;
    border:none;
    color:white;
    font-size: 25px;
    font-weight: bold;
    letter-spacing: 4px;
    box-shadow: 0px 8px 16px 0px inset rgba(0,0,0,0.2);

}
.input1{

  background-color: black;
  width: 75%;
  height: 30px;
  border: 1px solid gray;
  background-color: white;
  float:left;
  margin-left: 20px;
}
.html{
  width: 74.6%;
  height: 100px;
  border: 1px solid gray;
  background-color: white;
  margin-top: 0px;
  margin-left: 20px;
}
.p3 {
    width: auto;    
    margin-left: 20px;
    color:black;
    letter-spacing: 2px;    
    word-spacing: 2px ;
    font-size: 23px;
    margin-right: 0px;
}
.div-text{
    background-color: black;
    margin: auto;
    display: inline;
    width: 10000px;
    height: 20px;
}
.submit1{
    border: 1px solid black;
    font-size: 20px;
    background-color: lightgray;
    margin-left: 20px;
    padding: 3px 8px;
    margin-top: 20px;

}
.p5 {
    width: 100%;    
    color:black;
    letter-spacing: 2px;    
    word-spacing: 2px ;
    font-size: 27px;
    margin-right: 0px;
    margin-bottom: -20px;
    color:#1888e1;
    font-variant: small-caps;
    font-weight: bold;
    margin-top: 5px;

}
</style>

<body>   	
<div class="body-content">
	<div class="body-img">
<div class="navbar"> 
  <div class="dropdown">
    <button style="font-weight: bold;" class="dropbtn"> Menu  
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a class="navbara" href="landing.php">Home</a>
    </div>
  </div>
  <div class="nav-div"> 
    <button id="myBtn" class="sign btn-login"> Log In 
      <i class="fa fa-caret-down"></i>
    </button>
   </div>
    <p style="margin-top: 30px;float: right;" ><i>or</i></p>
   <div class="nav-div">
   <a  class="sign btn-sign-up" href="register.blade.php"> Sign Up</a> 
     	 <i class="fa fa-caret-down"></i>
    </div>
   </div>		
<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
   <div class="body-info1"> 
	   <p ><span class="about">ABOUT</span></p><br>
	   <div class="margin-left">
	   <h1 class="h1">Share Your Code And Learn (Online)</h1><br>
	   <p class="p"> By Squad Troops - 02/24/18 08:40 PM </p><br>
   		</div>
   </div>
</div>
</div>
<div  class="body-info">
	   <p class="p2 margin-left2"> <span style="color:#7abd13;font-size: 50px;">H</span>ello everyone! This website allows you to share your code in HTML, CSS, and Javascript in different kinds of format or templates. Create your own code and tell how it works.  Sign up now or Log in for more Codes</p><br><br><br><br>
	   <div><p class="posts"> By Squad Troops - 02/24/18 08:40 PM <br></p></div>

</div>
<!-- The Modal -->
<div id="myModal" class="the-modal">

  <!-- Modal content -->
  <div class="the-modal-content">
    <span class="close">&times;</span><br>
    <div class="modal-login">
    <form class="form" action="landing.php" method="post" enctype="multipart/form-data" autocomplete="off">
      <input class="input" type="email" placeholder="Email" name="email" required />
      <input class="input" type="password" placeholder="Password" name="password" autocomplete="new-password" required />
      <input class="submit" type="submit" value="LOG IN" name="register" class="btn btn-block btn-primary" />
     </form>
    </div>
</div>
</div>
</body>
 <script type="text/javascript">
   // Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>